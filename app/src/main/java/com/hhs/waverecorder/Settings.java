package com.hhs.waverecorder;

public final class Settings {

    public static String HOST = "120.126.151.155";
    public static String PORT = ":5000";
    public static String URL = "http://" + HOST + PORT;

    private Settings() {}
}
